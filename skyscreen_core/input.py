import logging
import zmq

class PatternParams(object):
	p1 = 0
	p2 = 0
	p3 = 0
	p4 = 0

class ZMQPatternParamsRecv(PatternParams):
	def __init__(self, receiving_port):
		self.receiving_port = receiving_port
		self.context = None

	def __enter__(self):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.SUB)
		self.socket.connect("tcp://127.0.0.1:%d" % self.receiving_port)
		self.socket.setsockopt_string(zmq.SUBSCRIBE, u"")
		self.poller = zmq.Poller()
		self.poller.register(self.socket, zmq.POLLIN)

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.socket.close()
		self.context = None
		self.poller = None

	def read_params(self):
		ready = self.poller.poll(0.0001)
		if ready:
			msg = self.socket.recv_json()
			logging.debug("Input recv: %s", msg)
			self.p1 = msg['p1']
			self.p2 = msg['p2']
			self.p3 = msg['p3']
			self.p4 = msg['p4']


class ZMQPatternParamsSend(object):
	def __init__(self, server_port=None):
		self.server_port = server_port
		self.context = None

	def __enter__(self):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUB)
		if self.server_port:
			self.socket.bind("tcp://127.0.0.1:%d" % self.server_port)
		else:
			self.server_port = self.socket.bind_to_random_port("tcp://127.0.0.1")

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.socket.close()
		self.context = None
		self.poller = None

	def send_params(self, p1, p2, p3, p4):
		msg = {
			'p1': p1,
			'p2': p2,
			'p3': p3,
			'p4': p4
		}
		logging.debug("Input send: %s" % str(msg))
		self.socket.send_json(msg)


class DummyRecv(object):
	def __init__(self):
		self.p1 = 0
		self.p2 = 0
		self.p3 = 0
		self.p4 = 0

	def read_params(self):
		pass

	def __enter__(self):
		pass

	def __exit__(self, exc_type, exc_val, exc_tb):
		pass