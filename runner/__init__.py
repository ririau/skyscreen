import logging
import setproctitle
import random
import shlex
import tempfile
from plumbum import cli
import signal
import sys
import time
import patterns.cli
import yaml
import schema
import jinja2
import skyscreen_core.bluetooth_receiver
import skyscreen_core.input
import skyscreen_core.interface
import skyscreen_core.memmap_interface
import os
import patterns.cli
import threading
import multiprocessing


def check_command_template(s):
	jinja2.Template(s).render(filename="", lock_port=0, command_port=0)
	return True


def substitute_template(command, filename, lock_port, command_port):
	return jinja2.Template(command).render(filename=filename, lock_port=lock_port, command_port=command_port)


running_pids = set()


def exit_masterfully(pid):
	logging.warning("Sending SIGTERM to %d" % pid)
	os.kill(pid, signal.SIGTERM)
	print "Waiting for exit of %d" % pid
	killedpid, err = os.waitpid(pid, os.WNOHANG)
	assert killedpid == 0 or killedpid == pid, "Pid %d for kill of %d was neither zero nor expected pid" % (
		killedpid, pid)
	if killedpid == pid:
		print "Killed %d successfully" % pid
		return

	time.sleep(0.1)
	killedpid, err = os.waitpid(pid, os.WNOHANG)
	if killedpid == pid:
		print "Killed %d after 1 second" % pid
		return

	os.kill(pid, signal.SIGKILL)
	logging.warning("Waiting on process %d after SIGKILL", pid)
	os.waitpid(pid, 0)
	logging.warning("Process %d exited after SIGKILL", pid)



def killall(sig, frame):
	for pid in running_pids:
		try:
			logging.warning("Killing process %d at exit", pid)
			exit_masterfully(pid)
		except OSError:
			# This most likely means that something else
			# got to the process first and killed it.
			pass
	print "EXITING NOW"
	sys.exit(0)


class CleanupThread(threading.Thread):
	def __init__(self):
		super(CleanupThread, self).__init__()
		self.pid_queue = multiprocessing.Queue()

	def run(self):
		while True:
			pid = self.pid_queue.get()
			if pid is None:
				logging.warning("Cleanup thread is EXITING")
				return
			else:
				try:
					logging.warn("Killing process %d", pid)
					self.kill_process(pid)
					logging.warn("Killed %d", pid)
				except OSError:
					pass

				try:
					print "Removing: %d" % pid
					running_pids.remove(pid)
				except KeyError:
					print "ERROR %d: %s" % (os.getpid(), str(running_pids))

	def kill_process(self, pid):
		os.kill(pid, signal.SIGTERM)
		killedpid, err = os.waitpid(pid, os.WNOHANG)
		assert killedpid == 0 or killedpid == pid, \
			"Pid %d for kill of %d was neither zero nor expected pid" % (
				killedpid, pid)
		if killedpid == pid:
			return
		time.sleep(0.1)
		killedpid, err = os.waitpid(pid, os.WNOHANG)
		if killedpid == pid:
			return
		os.kill(pid, signal.SIGKILL)
		logging.warning("Waiting on process %d after SIGKILL", pid)
		os.waitpid(pid, 0)
		logging.warning("Process %d exited after SIGKILL", pid)


config_schema = schema.Schema({
	'commands': [{'command': check_command_template}],
	'serial port': str,
	'use serial': bool,
	'max misses': int
})


@patterns.cli.PatternPlayer.subcommand("Runner")
class SkyscreenRunner(cli.Application, patterns.cli.PatternPlayerMixin):
	"""
	SkyscreenRunner is a high-level program.

	It takes a config file which includes how to run all the
	patterns as commands.

	Each command line should include the following variables that are templated in

	- The file that it can use for mmap
	- The ZMQ port

	As this is a list of patterns, the "A"  pattern will be the first in the list
	and the "B" pattern the second

	There is also a watchdog process. If either of the patterns fails to unlock for
	one 5th of a second, then that pattern is killed and replaced with another at random.

	Finally, this is a pattern itself, so it actually pulls in the pattern player mixin,
	with equivalent semantics, but with a very different main() function.
	"""

	def main(self, config_file_path):
		logging.warning("Using config: %s", os.path.abspath(config_file_path))
		logging.warning("Dump of the config follows")
		logging.warning("==========================")
		with open(config_file_path) as f:
			for line in f.readlines():
				logging.warning(line.strip())

		try:
			with open(config_file_path) as f:
				config = yaml.safe_load(f)
				config_schema.validate(config)
		except:
			logging.error("There was a problem with configuration\n"
						  "review the error and make the appropriate fix")
			raise
		self.commands = config['commands']
		self.max_misses = config['max misses']
		port = config['serial port']
		if config['use serial']:
			self.receiver = skyscreen_core.bluetooth_receiver.SerialInput(port)
		else:
			self.receiver = None

		self.main_from_renderer(self.two_command_renderer_inner)

	def two_command_renderer_inner(self, renderer, _):
		signal.signal(signal.SIGTERM, killall)
		self.cleanup_thread = CleanupThread()
		self.cleanup_thread.start()

		command_a = CommandContainer(self.commands[0], self.cleanup_thread.pid_queue)
		command_b = CommandContainer(self.commands[1], self.cleanup_thread.pid_queue)
		misses_a = 0
		misses_b = 0

		render_buf = renderer.__enter__()
		ca = command_a.__enter__()
		cb = command_b.__enter__()
		try:
			while True:
				frameA = command_a.start_read(25)
				frameB = command_b.start_read(25)
				render_buf[:] = 0.5 * ca[:] + 0.5 * cb[:]
				if frameB:
					command_b.finish_read(
						random.randint(0, 100),
						random.randint(0, 100),
						random.randint(0, 100),
						random.randint(0, 100)
					)
					misses_b = 0
				else:
					misses_b += 1
				if frameA:
					command_a.finish_read(
						random.randint(0, 100),
						random.randint(0, 100),
						random.randint(0, 100),
						random.randint(0, 100)
					)
					misses_a = 0
				else:
					misses_a += 1

				if misses_a > self.max_misses:
					logging.warning("Command A exceeded max misses")
					command_a.__exit__(None, None, None)
					ca = command_a.__enter__()
					misses_a = 0
					print "%d: %s" % (os.getpid(), str(running_pids))
				if misses_b > self.max_misses:
					logging.warning("Command B exceeded max misses")
					command_b.__exit__(None, None, None)
					cb = command_b.__enter__()
					misses_b = 0
					print "%d: %s" % (os.getpid(), str(running_pids))
				if random.random() < 0.005:
					if random.random() < 0.5:
						logging.warning("Randomly replacing command A")
						command_a.__exit__(None, None, None)
						command_a = CommandContainer(
							random.choice(self.commands),
							self.cleanup_thread.pid_queue)
						ca = command_a.__enter__()
						logging.warning("Randomly replaced command A")
						misses_a = 0
					else:
						logging.warning("Randomly replacing command B")
						command_b.__exit__(None, None, None)
						command_b = CommandContainer(
							random.choice(self.commands),
							self.cleanup_thread.pid_queue)
						cb = command_b.__enter__()
						logging.warning("Randomly replaced command B")
						misses_b = 0

				renderer.frame_ready()
		finally:
			command_b.__exit__(None, None, None)
			command_a.__exit__(None, None, None)
			renderer.__exit__(None, None, None)
			self.cleanup_thread.pid_queue.put(None)
			self.cleanup_thread.join()
			print "Exiting the two command renderer, pid: %d" % os.getpid()


class CommandContainer(object):
	def __init__(self, command, cleanup_queue):
		self.cleanup_queue = cleanup_queue
		self.command = command['command']
		self.sharedfile = tempfile.NamedTemporaryFile()
		writer = skyscreen_core.memmap_interface.NPMMAPScreenWriter(
			self.sharedfile.name, None)
		with writer as _:
			pass

	def __enter__(self):
		self.lock = skyscreen_core.interface.ZMQReaderSync(port=None)
		self.lock_port = self.lock.port

		self.variables = skyscreen_core.input.ZMQPatternParamsSend()
		self.variables.__enter__()
		self.variables_port = self.variables.server_port

		self.reader = skyscreen_core.memmap_interface.NPMMAPScreenReader(
			self.sharedfile.name, self.lock)

		self.pid = os.fork()
		if self.pid:
			logging.warning("Spawned process: %d from %d", self.pid, os.getpid())
			global running_pids
			running_pids.add(self.pid)
			return self.reader.__enter__()
		else:
			templated_command = substitute_template(
				self.command,
				self.sharedfile.name,
				self.lock_port,
				self.variables_port)
			call = shlex.split(templated_command)
			logging.warning("Executing: %s", str(call))
			os.execvp(call[0], call)
			assert False, "UNREACHABLE"

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.cleanup_queue.put(self.pid)
		self.reader.__exit__(exc_type, exc_val, exc_tb)
		self.variables.__exit__(exc_type, exc_val, exc_tb)
		self.lock.close()

	def start_read(self, timeout=None):
		return self.reader.start_read(timeout)

	def finish_read(self, p1, p2, p3, p4):
		self.variables.send_params(p1, p2, p3, p4)
		return self.reader.finish_read()

