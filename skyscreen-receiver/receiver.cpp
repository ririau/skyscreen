//
// Created by riri on 5/07/15.
//
#include <assert.h>
#include <errno.h>
#include <iostream>
#include <msgpack.hpp>
#include <netdb.h>
#include "receiver.h"
#include <stdarg.h>
#include <zlib.h>

typedef struct ServerS {
    Frame buffer[8];
    unsigned int output_frame_ix;
    unsigned int current_frame_ix;

    unsigned char msg[65507];
    unsigned char msg_decomp[65507];
    int sockfd;

    long state_vec;

    unsigned char n_nextframes;
    unsigned char n_ignored;

    // While frame_lock is held, you're
    // guaranteed that output_frame will
    // not change. Please use the frame_pause()
    // function to lock/unlock the mutex.
    pthread_mutex_t frame_lock;

    bool fast_mode;
} ServerS;


unsigned long int max_msg_len = 65507;
void process_message(Server s, char * msg_decomp, unsigned long received_uncompressed);
void simple_process_message(Server s, char * msg_decomp, unsigned long received_uncompressed);

static void die (int line_number, const char * format, ...) {
    va_list vargs;
    va_start (vargs, format);
    fprintf (stderr, "%d: ", line_number);
    vfprintf (stderr, format, vargs);
    fprintf (stderr, ".\n");
    exit (1);
}

Frame new_frame() {
    Frame f = (unsigned char *)calloc(FRAME_ITEMS, sizeof(unsigned char));
    assert(f && "Allocation of frame failed!");
    return f;
}

void free_frame(Frame f) {
    free(f);
}

Server new_server(bool fast_mode) {
    Server s = (ServerS *)calloc(1, sizeof(ServerS));
    assert(s != NULL);
    for (int i = 0; i < 8; i++)
        s->buffer[i] = new_frame();
    s->output_frame_ix = 0;
    s->current_frame_ix = 1;

    if (pthread_mutex_init(&(s->frame_lock), NULL) != 0)
        die(__LINE__, "Could not allocate frame lock");
    s->fast_mode = fast_mode;

    return s;
}

void free_server(Server s) {
    for (int i = 0; i < 8; i++) free_frame(s->buffer[i]);

    if (pthread_mutex_trylock(&(s->frame_lock)))
        die(__LINE__, "Could not lock frame lock for safe deallocation");
    pthread_mutex_unlock(&(s->frame_lock));
    pthread_mutex_destroy(&(s->frame_lock));
    free(s);
}

void initalize_server(Server s, const char* port) {
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_PASSIVE|AI_ADDRCONFIG;

    struct addrinfo* res = 0;
    int err = getaddrinfo("0", port, &hints, &res);

    if (err != 0) {
        die(__LINE__, "%s, failed to resolve local socket address (err=%d)", __FILE__, err);
    }
    int fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (fd==-1) {
        die(__LINE__, "%s: %s", __FILE__, strerror(errno));
    }
    if (bind(fd, res->ai_addr, res->ai_addrlen) == -1) {
        die(__LINE__, "%s: %s", __FILE__, strerror(errno));
    }
    freeaddrinfo(res);
    s->sockfd = fd;
}

void terminate_server(Server s) {
    close(s->sockfd);
}


typedef std::map<std::string, msgpack::object> MapStrMsgPackObj;

void receive_frames(Server s) {
    assert(s->sockfd);

    unsigned long received = recv(s->sockfd, s->msg, max_msg_len, 0);
    unsigned long received_uncompressed = max_msg_len;
    int err = uncompress(s->msg_decomp, &received_uncompressed, s->msg, received);
    if (err == Z_OK) {
        //simple_process_message(s, (char *)(s->msg_decomp), received_uncompressed);
        process_message(s, (char *)(s->msg_decomp), received_uncompressed);
    } else {
        printf("BAD MESSAGE\n");
    }
}

int map_to_circlebuffer(int output_frame_ix, int current_frame_ix, int i){
    int result;
    if ((current_frame_ix + i) % 8 == output_frame_ix)
        result = (current_frame_ix + i + 1) % 8;
    else
        result = (current_frame_ix + i) % 8;
    if (result < 0) result = 8 - ((-result) % 8);
    if (result < 0 || result >= 8)
        die(__LINE__, "Result was %d for op %d, cur %d and delta %d\n", result, output_frame_ix, current_frame_ix, i);
    //printf("Result was %d for op %d, cur %d and delta %d\n", result, output_frame_ix, current_frame_ix, i);
    return result;
}

inline void advance_frame(Server s) {
    s->current_frame_ix = map_to_circlebuffer(
        s->output_frame_ix, 
        s->current_frame_ix, 
        1);
}


void update_frame(Frame f, MapStrMsgPackObj mmap) {
    // Early exit if fields aren't found.
    if (!(mmap.count("start") &&
        mmap.count("end") &&
        mmap.count("vec"))) return;
    unsigned long start = mmap["start"].via.u64;
    unsigned long end = mmap["end"].via.u64;
    msgpack::object_raw data = mmap["vec"].via.raw;
    unsigned char* raw_chardata = (unsigned char*)data.ptr;
    //printf("%lu -> %lu\n", start, end);
    for (unsigned int i = 0;
         i + start < end &&
         i < data.size &&
         i < FRAME_SIZE;
         i++) {
        f[i+start] = raw_chardata[i];
    }
}
void simple_process_message(Server s,
                     char * msg_decomp,
                     unsigned long received_uncompressed) {
    std::string str(msg_decomp, received_uncompressed);

    msgpack::unpacked result;
    msgpack::unpack(&result, str.data(), str.size());

    msgpack::object deserialized = result.get();

    MapStrMsgPackObj mmap = deserialized.as<MapStrMsgPackObj>();
    long state_vec = mmap["state_vec"].via.u64;
    bool is_eof = mmap.count("start") == 0;
    if (!is_eof) {
        update_frame(s->buffer[s->output_frame_ix], mmap);
    }
}

void process_message(Server s,
                     char * msg_decomp,
                     unsigned long received_uncompressed) {
    std::string str(msg_decomp, received_uncompressed);

    msgpack::unpacked result;
    msgpack::unpack(&result, str.data(), str.size());

    msgpack::object deserialized = result.get();

    MapStrMsgPackObj mmap = deserialized.as<MapStrMsgPackObj>();
    long state_vec = mmap["state_vec"].via.u64;
    bool is_eof = mmap.count("start") == 0;
    int message_target_delta = state_vec - s->state_vec;
    if (message_target_delta > 8) {
        // Reset to work on this frame
        // and print a debug message.
        fprintf(stderr, "Error: message delta was %d\n", message_target_delta);
        s->state_vec = state_vec;
    } else {
        if (is_eof) {
            //printf("EOF\n");
            // We've recieved an EOF, so we advance to
            // the next frame, regardless of anything.
            int message_target = map_to_circlebuffer(
                s->output_frame_ix, 
                s->current_frame_ix, 
                message_target_delta+1
            );
            advance_frame(s);
            s->state_vec = state_vec+1;
        } else {
            int message_target = map_to_circlebuffer(
                s->output_frame_ix, 
                s->current_frame_ix, 
                message_target_delta
            );
            /*printf("writing to %d, while output is %d and current is %d\n", 
                message_target, 
                s->output_frame_ix,
                s->current_frame_ix
            );*/
            update_frame(s->buffer[message_target], mmap);
        }
    }
}

Frame frame_pause(Server s) {
    // In the frame pause, we just go ahead and show
    // the frame immediately before the one being written

    pthread_mutex_unlock(&s->frame_lock);
    if (s->fast_mode) {
        int prev_frame = map_to_circlebuffer(
            s->output_frame_ix, 
            s->current_frame_ix, 
            -1
        );
        s->output_frame_ix = prev_frame;
    } else {
        int prev_frame = s->current_frame_ix;
        //printf("Copy to %d from %d\n", s->output_frame_ix, prev_frame);
        assert(s->output_frame_ix != prev_frame);
        memcpy(s->buffer[s->output_frame_ix], s->buffer[prev_frame], FRAME_SIZE);
    }
    pthread_mutex_lock(&s->frame_lock);
    return s->buffer[s->output_frame_ix];
}
Frame first_frame(Server s) {
    pthread_mutex_lock(&s->frame_lock);
    return s->buffer[s->current_frame_ix];
}
void last_frame(Server s) {
    pthread_mutex_unlock(&s->frame_lock);
}

int main_(int argv, const char* argc[]) {
    Server s = new_server(0);
    std::string port = "5555";
    initalize_server(s, port.c_str());

    receive_frames(s);
    terminate_server(s);
    free_server(s);
    return 0;
}
